#include <glib-unix.h>
#include <gst/gst.h>
#include <gst/rtsp-server/rtsp-server.h>

#define RTSP_PORT "8554"
#define DEFAULT_PATH "/stream"

static char *path = (char *)DEFAULT_PATH;

static GOptionEntry entries[] = {
    {"path", 'p', 0, G_OPTION_ARG_STRING, &path,
     "Path to serve stream on (default: " DEFAULT_PATH ")", "PATH"},
    {NULL}};

static gboolean sigint_handler (gpointer user_data) {
  g_print("Caught SIGINT, shutting down....\n");
  exit(EXIT_SUCCESS);
}

int main(int argc, char *argv[]) {
  GMainLoop *loop;
  GstRTSPServer *server;
  GstRTSPMountPoints *mounts;
  GstRTSPMediaFactory *factory;
  GOptionContext *optctx;
  GError *error = NULL;
  guint signals;

  optctx = g_option_context_new("");
  g_option_context_add_main_entries(optctx, entries, NULL);
  g_option_context_add_group(optctx, gst_init_get_option_group());
  if (!g_option_context_parse(optctx, &argc, &argv, &error)) {
    g_printerr("Error parsing options: %s\n", error->message);
    g_option_context_free(optctx);
    g_clear_error(&error);
    return -1;
  }
  g_option_context_free(optctx);

  signals = g_unix_signal_add (SIGINT, (GSourceFunc) sigint_handler, NULL);

  loop = g_main_loop_new(NULL, FALSE);

  server = gst_rtsp_server_new();
  g_object_set(server, "service", RTSP_PORT, NULL);

  mounts = gst_rtsp_server_get_mount_points(server);

  factory = gst_rtsp_media_factory_new();
  gst_rtsp_media_factory_set_launch(factory, argv[1]);
  gst_rtsp_media_factory_set_shared(factory, TRUE);

  gst_rtsp_mount_points_add_factory(mounts, path, factory);

  g_object_unref(mounts);

  gst_rtsp_server_attach(server, NULL);

  g_print("stream ready at %s\n", path);
  g_main_loop_run(loop);

  return 0;
}
