# syntax=docker/dockerfile:1

FROM docker.io/library/ubuntu:rolling AS builder

WORKDIR /src

RUN apt-get update && \
    DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends gcc libgstreamer1.0-dev libgstrtspserver-1.0-dev

COPY src /src

RUN gcc -o rtsp-streamer main.c \
    $(pkg-config --cflags --libs gstreamer-rtsp-server-1.0)

FROM docker.io/library/ubuntu:rolling

ARG build_time
ARG git_revision
ARG quay_expires_after=2w

LABEL quay.expires-after=$quay_expires_after
LABEL org.opencontainers.image.created=$build_time
LABEL org.opencontainers.image.source=https://gitlab.com/ioanrogers/rtsp-streamer.git
LABEL org.opencontainers.image.licenses=AGPL-3.0
LABEL org.opencontainers.image.revision=$git_revision

ENTRYPOINT ["/rtsp-streamer"]

EXPOSE 8554

RUN apt-get update && \
    DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends libgstrtspserver-1.0-0

COPY --from=builder /src/rtsp-streamer ./

ENV GST_DEBUG="*:4"
ENTRYPOINT ["/rtsp-streamer"]
CMD ["--path=/stream1", "v4l2src device=/dev/video0 do-timestamp=true ! videoconvert ! rtpvrawpay name=pay0"]
